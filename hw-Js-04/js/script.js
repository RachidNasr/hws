document.write("<div class='code'>");

//1-
document.write("<span class='oblique'>1- map(fn, array). </span><br>");
let array = [];

//Function declaration
function map(fn, array) {
    return document.write("[" + fn(array) + "]");
}

map(function (array) {
    let arr = [];
    for (let i = 0; i < array.length; i++) {
        arr[i] = Math.pow(array[i], 2);
    }
    return arr;
}, [1, 2, 3, 4, 5, 6, 7, 8, 9]);


document.write("<br><br>");

//2-Перепишите функцию, используя оператор '?' или '||.
document.write("<span class='oblique'>2-Перепишите функцию, используя оператор '?' или '||. </span><br>");

//2.1
function checkAge1(age) {
    return (age > 18) ? true : confirm('Родители разрешили?');
}

document.write("checkAge1(20) : <strong>" + checkAge1(20) + "</strong>");
document.write("<br>");

//2.2
let checkAge2 = (age) => (age > 18) || confirm('Родители разрешили?');

document.write(`checkAge2(17) : <strong>${checkAge2(17)} </strong>`);


document.write("</div>");