document.write("<div class='code'>");
document.write("<p><strong>Задание №1:</strong><br>");
/*************
Разработайте функцию-конструктор, которая будет создавать объект Human(человек). 
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива по значению свойства Age по возрастанию или по убыванию.
**************/

//create constructor
function  Human(name, age) {
    this.name = name;
    this.age = age;
}

const arr =[
    new Human("Alex", 70),
    new Human("Ivan", 30),
    new Human("Farid", 40),
    new Human("Samir", 19),
];
document.write(`${JSON.stringify(arr)} <br>`);
arr.sort((a, b) => a.age - b.age);

document.write(`<span class='oblique'>After Sorting</span><br>`);
document.write(JSON.stringify(arr));

//
document.write("<p><strong>Задание №2:</strong><br><br>");
/************** 
Разработайте функцию-конструктор, которая будет создавать объект
Human(человек), добавьте на свое усмотрение свойство и методы в этот объект. Подумайте, какие методы и свойства следует сделать на уровне экземпляра, а какие уровня функции-конструктора.
*************/
//create the constructor
//You cannot have two constructors with the exact same name that's why we'll use Human1
function Human1(firstName, lastName, job) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.job = job;

}
let worker = new Human1("Ivan", "Ivanov", "Developer");
Human1.getWorker = function () {
    return new Human1("Farid", "Hamdi", "Adminastrator");
}

function data() {
    this.firstName = prompt('Введите ваше имя');
    this.lastName = prompt('Введите вашу фамилию');
    this.job = (prompt('Введите вашу должность'));
}
function  show() {
    document.write(`<p class='worker'>First Name : ${this.firstName}<br>`);
    document.write(`Last Name : ${this.lastName}<br>`);
    document.write(`Job : ${this.job}<br></p>`);
}

Human1.getWorker();

data();
show();

document.write("</div>");
