import React from "react";
import '../css/style.css';
import { FaEnvelope } from "react-icons/fa";


const Footer = () => {
    return(
        <div className='footer-wrapper'>
            <div className='container'>
                <div className='footer-content'>
                    <div className='footer-text'>
                        <p className='footer-title'>САМІ РОЗУМНІ ПРОЄКТИ</p>
                        <p className='footer-subtitle'>РОЗРОБЛЮЄМО САМІ СМІЛИВІ ПРОЕКТИ В УКРАЇНІ</p>
                    </div>
                    <div className='footer-email'>
                        <FaEnvelope />
                        <p className='footer-email-text'>ВАШ ЗАПИТ</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;