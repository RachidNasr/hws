import React from "react";
import '../css/style.css';
import img1 from '../images/pic-1.jpg';
import img2 from '../images/pic-2.jpg';
import img3 from '../images/pic-3.jpg';

const Body = () => {
    return(
        <div className='about-wrapper'>
            <h3 className='about-title'>
                Наші самі великі проєкти
            </h3>
            <div className='about-content'>
                <div className='about-item'>
                    <img className='about-img' src={img1}/>
                    <span></span>
                    <p className='about-item-title'>Земля</p>
                    <p className='about-item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
                <div className='about-item'>
                    <img className='about-img' src={img2}/>
                    <span></span>
                    <p className='about-item-title'>Марс</p>
                    <p className='about-item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
                <div className='about-item'>
                    <img className='about-img' src={img3}/>
                    <span></span>
                    <p className='about-item-title'>Сатурн</p>
                    <p className='about-item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
            </div>
        </div>
    )
}



export default Body;