import React from "react";
import '../css/style.css';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';


const App = () => {
    return (
        <div className='wrapper'>
            <div className='container'>
                <Header />
                <Body />
            </div>
            <Footer />
        </div>
    )
}

export default App;