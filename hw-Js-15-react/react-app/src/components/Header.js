import React from "react";
import '../css/style.css';
import LogoImg from '../images/logo.svg';
import HMIcon from '../images/HMIcon.svg';
import { GiHamburgerMenu } from "react-icons/fa";

const Header = () => {
    return (
        <div>
            <div className='header-top'>
                <img className='logo' src={LogoImg} alt='logo'/>
                <div className='menu'>
                    <button className='header-btn'>
                        {/* <GiHamburgerMenu /> */}
                        <img className='menu-logo' src={HMIcon} alt='Hamburger menu icon'/>
                    </button>
                </div>
            </div>
            <div className='header-content'>
                <p className='header-title'>
                СТВОРЮЄМО ВЕЛИКІ
                ПРОЄКТИ В УКРАЇНІ
                </p>
                <p className='header-subtitle'>стадионы, газопроводы, мосты, дамбы</p>
            </div>
            <div className='numbers-wrapper'>
            <div className='numbers-content'>
                <div className='numbers-item'>
                    <p className='number'>10</p>
                    <p className='numbers-title'>Років</p>
                    <p className='numbers-subtitle'>з вами</p>
                </div>
                <div className='numbers-item'>
                    <p className='number'>500+</p>
                    <p className='numbers-title'>Проєктів</p>
                    <p className='numbers-subtitle'>зроблено</p>
                </div>
                <div className='numbers-item'>
                    <p className='number'>400+</p>
                    <p className='numbers-title'>клієнтів</p>
                    <p className='numbers-subtitle'>в захваті</p>
                </div>
                <div className='numbers-item'>
                    <p className='number'>30</p>
                    <p className='numbers-title'>Хвилин</p>
                    <p className='numbers-subtitle'>презентація</p>
                </div>
            </div>
        </div>
        </div>
        
        
    )
}

export default Header;