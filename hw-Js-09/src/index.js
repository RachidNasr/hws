/*
2-Создайте приложение секундомер.
-Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
-При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый 
Вывод счётчиков в формате ЧЧ:ММ:СС
Реализуйте Задание используя синтаксис ES6 и стрелочные функции 
*/
let startState = false,
    starter, sec, min, millisec,
    milliseconds = 0,
    seconds = 0,
    minutes = 0;

function showTime() {
    milliseconds++;
    if (minutes >= 60) {
        clearTimeout(starter);
    }
    if (seconds >= 60) {
        minutes++;
        seconds = 0;
    }
    if (milliseconds >= 60) {
        seconds++;
        milliseconds = 0;
    }
    if (minutes < 10) {
        min = '0' + minutes;
    } else {
        min = minutes;
    }
    if (seconds < 10) {
        sec = '0' + seconds;
    } else {
        sec = seconds;
    }
    if (milliseconds < 10) {
        millisec = '0' + milliseconds;
    } else {
        millisec = milliseconds;
    }
    document.querySelector('.stopwatch-display').innerHTML = `${min}:${sec}:${millisec}`;
}

document.querySelector('.stopwatch-control button:nth-child(1)').onclick = () => {
    if (!startState) {
        starter = setInterval(showTime, 10);
        startState = true;
    }
    document.querySelector('.container-stopwatch').classList.remove('red-js');
}
document.querySelector('.stopwatch-control button:nth-child(2)').onclick = () => {
    clearInterval(starter);
    startState = false;
    document.querySelector('.container-stopwatch').classList.add('red-js');
}
document.querySelector('.stopwatch-control button:nth-child(3)').onclick = () => {
    document.querySelector('div.stopwatch-display').innerHTML = '00:00:00'
    minutes = seconds = milliseconds = 0;
    document.querySelector('.container-stopwatch').classList.remove('red-js');
}

/*
3-Реализуйте программу проверки телефона
-Используя JS Создайте поле для ввода телефона и кнопку сохранения
-Пользователь должен ввести номер телефона в формате 000-000-00-00
-Поле того как пользователь нажимает кнопку сохранить проверте правильность ввода номера, если 
номер правильный сделайте зеленый фон и используя document.location переведите пользователя на 
страницу https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg 
если будет ошибка отобразите её в диве до input.
*/

const reg = document.querySelector('.reg-container');

// phone label
const regLabel = document.createElement('label');
regLabel.setAttribute('for', 'phoneNumber');
regLabel.textContent = 'Введите номер телефона';
reg.append(regLabel);

// Error div
const errorDiv = document.createElement('div');

// textinput style and attributes
const regInput = document.createElement('input');
regInput.classList.add('reg-input');
regInput.setAttribute('type', 'tel');
regInput.setAttribute('id', 'phoneNumber');
regInput.setAttribute('placeholder', '000-000-00-00')
reg.append(regInput);

// button attributes
const regBtn = document.createElement('input');
regBtn.classList.add('reg-btn');
regBtn.setAttribute('type', 'button');
regBtn.setAttribute('value', 'Сохранить');
reg.append(regBtn);

// reg expression
const pattern = /\d\d\d-\d\d\d-\d\d-\d\d/;

// testing reg expression
regBtn.onclick = () => {
    if (regInput.value.length == 13 && pattern.exec(regInput.value)) {
        errorDiv.classList.remove('error');
        errorDiv.classList.add('passed');
        regInput.classList.add('reg-input-passed');
        errorDiv.innerText = `Congratulations Successfully Passed :)`;
        setTimeout(() => document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg', 500);
    } else {
        regLabel.remove();
        errorDiv.innerText = '';
        errorDiv.classList.add('error');
        regInput.classList.add('red-js');
        errorDiv.innerText = `Ошибка :( Не правильный номер.попробуйте еще раз `;
        regInput.before(errorDiv);
    }
}

/*
4-Создайте слайдер который каждые 3 сек будет менять изображения
Изображения для отображения
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/

let imageIndex = 0;
const myImage = document.querySelector('.slider .img-wrap img');
const images = [
    'https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
    'https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
    'https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg',
    'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg',
];

function slideShow() {
    myImage.setAttribute('src', images[imageIndex]);
    imageIndex++;
    setTimeout(slideShow, 3000);
    if (imageIndex === images.length)  imageIndex = 0;
}
slideShow();