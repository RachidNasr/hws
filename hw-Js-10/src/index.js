/****************************************HW-JS-10***********************************************
-В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
- При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
- При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит 
- программа ждет введения второго числа для выполнения операции.
- Если пользователь ввел одно число, выбрал оператор, и ввел второе число, 
то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат 
выполенния предыдущего выражения.
- При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву 
`m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране.
 Повторное нажатие `MRC` должно очищать память.
************************************************************************************************/
const calc = {
    operand1: "",
    operand2: "",
    sign: "",
    result: "",
    mem: ""
};

window.addEventListener("DOMContentLoaded", () => {
    const btns = document.querySelector(".keys");
    btns.addEventListener("click", click);
})

const click = (e) => {
    // console.log(e.target.value);
    if (/[0-9.]/.test(e.target.value) && calc.sign === "") {
        calc.operand1 += e.target.value;
        show(calc.operand1);

    } else if (/[0-9.]/.test(e.target.value) && calc.sign !== "") {
        calc.operand2 += e.target.value;
        show(calc.operand2);

    } else if (/[+*-/]/.test(e.target.value)) {
        calc.sign = e.target.value;
        show(calc.sign);

    } else if (/[=]/.test(e.target.value)) {
        calc.result = executeOperation(calc.operand1, calc.operand2, calc.sign)
        document.querySelector(".display > input").value.innerText = calc.result;
        show(calc.result);
        calc.operand1=calc.operand2=calc.sign ='';

    } else if (/[C]/.test(e.target.value)) {
        clear();

    } else if (/[m+]/.test(e.target.value)) {
        // console.log("m+ pressed " + calc.mem);
        calc.mem += parseFloat(calc.operand1);
        document.querySelector(".display > input").value = calc.mem;
        document.querySelector(".keys input:nth-child(1)").style.backgroundColor = "green";
        
    } else if (/[m-]/.test(e.target.value)) {
        // console.log("m- pressed " + calc.mem);
        calc.mem -= parseFloat(calc.operand1);
        document.querySelector(".display > input").value = calc.mem;
        document.querySelector(".keys input:nth-child(1)").style.backgroundColor = "green";

    } else if (/[mrc]/.test(e.target.value)) {
        // console.log("MRC pressed " + calc.mem);
        document.querySelector(".display > input").value = calc.mem;

    } else {
        console.error("error");
    }
}

const show = (data) => {
    document.querySelector(".display > input").value = data;
}


const executeOperation = (n1, n2, sign) => {
    switch (sign) {
        case "+":
            return parseFloat(n1) + parseFloat(n2);
        case "-":
            return n1 - n2;
        case "*":
            return n1 * n2;
        case "/":
            return n1 / n2;
    };
};


const clear = () => {
    calc.operand1 = "";
    calc.operand2 = "";
    calc.sign = "";
    calc.mem = "";
    document.querySelector(".display > input").value = "";
    document.querySelector(".keys input:nth-child(1)").style.backgroundColor = "gray";

};
