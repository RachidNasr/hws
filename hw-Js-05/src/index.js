document.write("<div class='code'>");
const documentX = {
    header : "",
    body : "",
    footer : "",
    data : "",
    application :{
            header : {},
            body : {},
            footer : {},
            data : {},
    },
    fill : function () {
        this.header = prompt("fill in your header", "header");
        this.body = prompt("fill in your body", "body");
        this.footer = prompt("fill in your footer", "footer");
        this.data = prompt("fill in your data", "data");
        this.application.header = prompt("fill in your app header", "application header");
        this.application.body = prompt("fill in your app body", "application body");
        this.application.footer = prompt("fill in your app footer", "application footer");
        this.application.data = prompt("fill in your app data", "application data");

    },
    show : function () {
        document.write(`<h1>${this.header}</h1>`);
        document.write(`<p>${this.body}</p>`);
        document.write(`<p>${this.footer}</p>`);
        document.write(`<p>${this.data}</p>`);
        document.write(`<div class='app'><h2>${this.application.header}</h2>`);
        document.write(`<p>${this.application.body}</p>`);
        document.write(`<p>${this.application.footer}</p>`);
        document.write(`<p>${this.application.data}</p></div>`);
    }
};

documentX.fill();
documentX.show();
document.write("</div>");
