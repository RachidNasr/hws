document.addEventListener('DOMContentLoaded', () => {

    const prices = {
        size: {
            small: 80,
            medium: 100,
            big: 120
        },
        sauce: {
            ketchup: 5.00,
            bbq: 10.25,
            ricotta: 7.50
        },
        topping: {
            cheese: {
                ordinary: 15.00,
                feta: 10.75,
                mozzarella: 15.30
            },
            veal: 25.00,
            tomato: 10.00,
            mushroom: 12.00
        }
    };
    // console.log(prices);

    const orderedPizza = {
        size: null,
        sauce: [],
        sauceName: [],
        topping: [],
        toppingName: [],
        totalPrice: null,
    };

    // Choosing pizza size
    const pizzaSize = document.getElementById('pizza');
    pizzaSize.addEventListener('click', checkSize);

    function checkSize({target}) {
        console.log(target.value);
        orderedPizza.size = target.value;
        showInfo();
    };

    // determining the price
    function totalPrice() {
        //reseting the radio buttons
        if (orderedPizza.totalPrice != 0) orderedPizza.totalPrice = 0;
        // determining the size
        switch (orderedPizza.size) {
            case 'small':
                orderedPizza.totalPrice += prices.size.small;
                break;
            case 'mid':
                orderedPizza.totalPrice += prices.size.medium;
                break;
            case 'big':
                orderedPizza.totalPrice += prices.size.big;
                break;
        };

        // sauce prices
        orderedPizza.sauce.forEach(element => {
            switch (element) {
                case "sauceClassic":
                    orderedPizza.totalPrice += prices.sauce.ketchup;
                    break;
                case "sauceBBQ":
                    orderedPizza.totalPrice += prices.sauce.bbq;
                    break;
                case "sauceRikotta":
                    orderedPizza.totalPrice += prices.sauce.ricotta;
                    break;
            }
        });

        // topping prices
        orderedPizza.topping.forEach(element => {
            switch (element) {
                case "moc1":
                    orderedPizza.totalPrice += prices.topping.cheese.ordinary;
                    break;
                case "moc2":
                    orderedPizza.totalPrice += prices.topping.cheese.feta;
                    break;
                case "moc3":
                    orderedPizza.totalPrice += prices.topping.cheese.mozzarella;
                    break;
                case "telya":
                    orderedPizza.totalPrice += prices.topping.veal;
                    break;
                case "vetch1":
                    orderedPizza.totalPrice += prices.topping.tomato;
                    break;
                case "vetch2":
                    orderedPizza.totalPrice += prices.topping.mushroom;
                    break;
            }
        });
    };

    // display pizza price
    function showPrice() {
        console.log("totalprice :" + orderedPizza.totalPrice);
        document.querySelector('.price > p').textContent = `Цiна: ${orderedPizza.totalPrice} грн.`;
    }
    // displaying sauces prices
    function showSauces() {
        console.log("namesauce : " + orderedPizza.sauceName);
        console.log("sauce : " + orderedPizza.sauce);
        document.querySelector('.sauces > p').textContent = `Соуси: ${orderedPizza.sauceName.join(', ')}`;
    };

    // displaying toppings prices
    function showToppings() {
        console.log("nametopping : " + orderedPizza.toppingName);
        console.log("topping : " + orderedPizza.topping);
        document.querySelector('.topings > p').textContent = `Топiнги: ${orderedPizza.toppingName.join(', ')}`;
    };

    // Show it all
    function showInfo() {
        totalPrice();
        showPrice();
        showSauces();
        showToppings();
    }

    // Drag and Drop
    const dragAndDrop = () => {
        const dragPizza = document.querySelector('.table-wrapper .table');

        function dragStart(event) {
            // Fetching sauce and toppings names from data attributes and pushing them to the Array
            // elements of the orderedPizza object.
            const extra = event.target.dataset.key;
            if (extra === 'sauce') {
                if (!orderedPizza.sauceName.includes(event.target.alt)) {
                    orderedPizza.sauceName.push(event.target.alt);
                    orderedPizza.sauce.push(event.target.id);
                }
            } else if (extra === 'topping') {
                if (!orderedPizza.toppingName.includes(event.target.alt)) {
                    orderedPizza.toppingName.push(event.target.alt);
                    orderedPizza.topping.push(event.target.id);
                }
            };

            event.dataTransfer.effectAllowed = 'move';
            event.dataTransfer.setData("img", this.attributes.src.textContent);
        };

        function dragEnd(event) {
            dragPizza.classList.remove('hovered');
            if (event.preventDefault) event.preventDefault();
            if (event.stopPropagation) event.stopPropagation();
        };

        function dragOver(event) {
            if (event.preventDefault) event.preventDefault();
            if (event.stopPropagation) event.stopPropagation();
        };

        function dragEnter(event) {
            dragPizza.classList.add('hovered');
            if (event.preventDefault) event.preventDefault();
            if (event.stopPropagation) event.stopPropagation();
        };

        function dragLeave(event) {
            dragPizza.classList.remove('hovered');
            if (event.preventDefault) event.preventDefault();
            if (event.stopPropagation) event.stopPropagation();
        };

        function dragDrop(event) {
            if (event.preventDefault) event.preventDefault();
            if (event.stopPropagation) event.stopPropagation();

            const imgIngridient = document.createElement('img');
            imgIngridient.setAttribute('src', event.dataTransfer.getData('img'));
            dragPizza.append(imgIngridient);

            showInfo();
        };
  
        dragPizza.addEventListener('dragover', dragOver);
        dragPizza.addEventListener('drop', dragDrop);
        dragPizza.addEventListener('dragenter', dragEnter);
        dragPizza.addEventListener('dragleave', dragLeave);
      
        const ingridients = document.querySelectorAll('.ingridients .draggable');
        ingridients.forEach(ingridient => {
            ingridient.addEventListener('dragstart', dragStart);
            ingridient.addEventListener('dragend', dragEnd);
        });

    }

    
    dragAndDrop();

    /***********************Banner*****************************/
    const banner = document.getElementById('banner');
    banner.addEventListener('mouseover', (event) => {
        banner.style.right = Math.random() * (document.documentElement.clientWidth - event.target.offsetWidth) + 'px';
        banner.style.bottom = Math.random() * (document.documentElement.clientHeight - event.target.offsetHeight) + 'px';
    });

    /******************Reset Button***************************** */
    document.getElementById('resetBtn').addEventListener('mousedown', event => {
        event.target.classList.add('btn-cliked');
    });
    document.getElementById('resetBtn').addEventListener('mouseup', event => {
        event.target.classList.remove('btn-cliked');
    });
    document.getElementById('resetBtn').addEventListener('click', () => {


        document.querySelectorAll('#pizza .radioIn').forEach(size => {
            size.checked = '';
        })

        const ingridients = document.querySelectorAll('.table img');
        ingridients.forEach(ingridient => {
            if (!ingridient.alt[0]) {
                ingridient.remove();
            }
        })

        // Emptying the object
        orderedPizza.size = null;
        orderedPizza.sauce = [];
        orderedPizza.sauceName = [];
        orderedPizza.topping = [];
        orderedPizza.toppingName = [];
        orderedPizza.totalPrice = null;

        showInfo();
    })

    /************************ Form VAlidation***************************/
const f1 = document.forms[1];
const btnSubmit = document.forms[1].elements[4];

const patternName = /[a-zа-ї]+/i;
const msgName = "Name not valid";

const patternTel = /\+380\d{9}/g;
const msgTel = "Phone number not valid";

const patternEmail = /\b\w+@[a-z]+\.[a-z]{2,4}\b/g;
const msgEmail ="Email address not valid";

const patterns = [patternName, patternTel, patternEmail];
const messegeError = [msgName, msgTel, msgEmail];

const elementsValue = []
const elementsPlaceholder = []

const clearError = () => {
    for (i = 0; i < f1.elements.length-2; i++) {
        if (f1.elements[i].style.backgroundColor === "#EF9A9A"){
            f1.elements[i].style.backgroundColor = "white";
            if (f1.elements[i].getAttribute("placeholder") === "This field must be filled ") {
                f1.elements[i].setAttribute("placeholder", elementsPlaceholder[i]);
            } else {
                f1.elements[i].setAttribute("placeholder", elementsPlaceholder[i]);
                f1.elements[i].value = elementsValue[i];
            }
            f1.elements[i].style.width = "300px";
        }
    }
    btnSubmit.addEventListener("click", showError, false);
}

const showError = (e) => {
    btnSubmit.removeEventListener("click", showError, false);
    let empty = false, invalid = false;
    for (i = 0; i < f1.elements.length-2; i++) {
        elementsValue[i] = f1.elements[i].value;
        elementsPlaceholder[i] = f1.elements[i].getAttribute("placeholder")
        if (f1.elements[i].value.length == 0) {
            f1.elements[i].style.backgroundColor = "#EF9A9A";
            f1.elements[i].style.color = "#fff";
            f1.elements[i].style.width = "300px";
            f1.elements[i].style.border = "4px solid #B71C1C";
            f1.elements[i].value = "";
            f1.elements[i].setAttribute("placeholder", "This field must be filled");
            empty = true;
        } else if (f1.elements[i].value.search(patterns[i])==-1) {
            f1.elements[i].style.backgroundColor = "#EF9A9A";
            f1.elements[i].style.color = "#fff";
            f1.elements[i].style.width = "300px";
            f1.elements[i].style.border = "4px solid #00C853";
            f1.elements[i].value = "";
            f1.elements[i].setAttribute("placeholder", messegeError[i]);
            invalid = true;
        } 
    }
    setTimeout(clearError, 3000)
    if (!empty && !invalid) {
        f1.elements[i].style.border = "4px solid #00C853";
        f1.elements[i].style.backgroundColor = "#69F0AE";
        document.location.href = "./thank-you.html";
    }
}
btnSubmit.addEventListener("click", showError, false);




});