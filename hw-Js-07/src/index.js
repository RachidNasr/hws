document.write("<div class='code'>");
/*********************** 
1-Реализовать функцию для создания объекта "пользователь".
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. 
При вызове функция должна спросить у вызывающего имя и фамилию. 
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName
 и lastName. Добавить в объект newUser метод getLogin(), который будет возвращать первую 
 букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре 
 (например, Ivan Kravchenko → ikravchenko). Создать пользователя с помощью функции 
 createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат
  выполнения функции.

2-Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее 
следующим функционалом: При вызове функция должна спросить у вызывающего дату рождения 
(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday. Создать метод getAge() который будет
 возвращать сколько пользователю лет. Создать метод getPassword(), который будет возвращать первую
  букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом 
  рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). Вывести в консоль результат 
  работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*********************/
class CreateNewUser {
    constructor(firstName, lastName, birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    };
    getLogin() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };
    getAge() {
        return new Date().getFullYear() - this.birthday.split(".")[2];
    };
    getPassword() {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2];
    };
};


const newUser = new CreateNewUser(
    prompt("Имя"),
    prompt("Фамилия"),
    prompt("Дата рождения в формате dd.mm.yyyy", "dd.mm.yyyy"));


console.log(newUser.getLogin());
document.write(`Login : <strong>${newUser.getLogin()}</strong><br>`);
console.log(newUser.getAge());
document.write(`Age : <strong>${newUser.getAge()}</strong><br>`);
console.log(newUser.getPassword());
document.write(`Password : <strong>${newUser.getPassword()}</strong><br>`);

/*******************
 3-Реализовать функцию фильтра массива по указанному типу данных.
Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. 
Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
 Функция должна вернуть новый массив, который будет содержать в себе все данные, 
 которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. 
 То есть, если передать массив ['hello', 'world', 23, '23', null], 
 и вторым аргументом передать 'string', то функция вернет массив [23, null].
 ***************************/


 let arr = ['hello', 'world', 23, '23', null];
 const filterBy = (array, type) => array.filter(item => typeof item !== type);
 
 console.log(filterBy(arr, "string"));
//  document.write(`filter : <strong>[${filterBy(arr, "string")}]</strong><br>`);

document.write("</div>");