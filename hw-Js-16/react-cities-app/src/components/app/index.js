import React from "react";
import data from '../../data';
import TableBody from "../table-body";
import TableHeader from "../table-header";

import './app.css';

// console.log(data);

function App() {
  return (
    // <h1>Hello React</h1>
    <table>
        <TableHeader colName={["N","Area", "Id", "ParentID"]}></TableHeader>
        <TableBody data={data.areas}></TableBody>
    </table>
  ) 
}

export default App;