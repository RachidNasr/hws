import React from "react";

const TableBody = ({data}) =>{
    console.log("data.areas",data);
    return(
        <tbody>
        {
            Array.isArray(data) ?
            data.map((item, index) => {
                return (
                    <tr key={index*2+"g"}>
                        <td>{index+1}</td>
                        <td>{item.name}
                        {
                            data[index].areas.map((el,i) => {
                                // console.log(el)
                                return <table><tr><td>{i+1+"-"}{el.name}</td></tr></table>
                                
                            })
                        }
                        </td>
                        <td>{item.id}</td>
                        <td>{item.parent_id}</td>
                    </tr>
                )
            })
            : null
        }
        </tbody>
    )
}

export default TableBody;