import React from "react";

const TableHeader = (props) => {
    console.log(props);
    return(
        <thead>
            <tr>
            {
                Array.isArray(props.colName) ? 
                props.colName.map((item,index) => {
                    return <th key={index*2+"u"}>{item}</th>
                })
                : null
            }
            </tr>
            </thead>
    )
}

export default TableHeader;