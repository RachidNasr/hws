
    //************************************* HW-JS-13***************************/
    const SWAP = {
        //All links in one object
        urls: {
          base: 'https://swapi.dev/api/',
          people: 'people/',
          planets: 'planets/',
          films: 'films/',
          species: 'species/',
          vehicles: 'vehicles/',
          starships: 'starships/',
        },
        init: () => {
          SWAP.addListeners();
          SWAP.buildNav();
        },
        addListeners: () => {
          let nav = document.getElementById('nav');
          nav.addEventListener('click', SWAP.getData);
          footer.addEventListener('click', SWAP.getData);
        },
        buildNav: () => {
          let df = new DocumentFragment();
          for (let nm in SWAP.urls) {
            // console.log(nm);
            if (nm == 'people') {
              let link = document.createElement('a');
              link.href = `${SWAP.urls.base}${SWAP.urls[nm]}`;
              link.textContent = nm;
              link.setAttribute('data-link', `${SWAP.urls.base}${SWAP.urls[nm]}`);
              df.append(link);
            
            }
          }
          document.getElementById('nav').append(df);
        },
        getData: (ev) => {
          if (ev) ev.preventDefault();
          //show overlay / loader
          document.querySelector('.overlay').classList.add('active');
          //get the url
          let link = ev.target;
          let url = link.getAttribute('data-link');
          console.log(`${SWAP.urls.base}planets/`);
          //fetch the data(people & planets)
          Promise.all([
          fetch(url).then((resp) => { return resp.json();}).then(SWAP.getPeople),
          fetch(`${SWAP.urls.base}planets/`).then((resp) => { return resp.json();}).then(SWAP.getPlanet)
        ]).then();
          //call the build function
        },
        getPlanet:(data)=>{
          localStorage.setItem('planets', JSON.stringify(data));
          localStorage.getItem('planets');
          // console.log(data);
          // sessionStorage.planet =JSON.stringify(data);
          // console.log(data.results);
          data.results.map(item=>{
            console.log("planet item",item.name);
            // localStorage.planet =JSON.stringify(data);
            return item;
          });
        },
        getPeople: (data) => {
         localStorage.setItem('peoples', JSON.stringify(data));
          let m = document.getElementById('main');
          console.log("People", data);
          // console.log(SWAP.urls);
          //hide the overlay / loader 
          document.querySelector('.overlay').classList.remove('active');
          //add the data
          
          m.innerHTML = data.results
            .map((item, index) => { 
              // console.log("item", item);
              // localStorage.people = JSON.stringify(data);        
              return `
              <div class="card" style="width: 18rem;">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Fullname</b>: ${item.name}</li>
                <li class="list-group-item"><b>Gender</b>: ${item.gender}</li>
                <li class="list-group-item"><b>Height</b>: ${item.height}</li>
                <li class="list-group-item"><b>Skin Color</b>: ${item.skin_color}</li>
                <li class="list-group-item"><b>Birth Year</b>: ${item.birth_year}</li>
                <li class="list-group-item"><b>Planet</b>: ${JSON.parse(localStorage.getItem('planets')).results[index].name}</li>
              </ul>

              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
              Save info :${item.name}
            </button>
          </div>
          
          <!-- The Modal -->
          <div class="modal" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
          
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">${JSON.parse(localStorage.getItem('peoples')).results[index].name}</h4>
                  <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
          
                <!-- Modal body -->
                
                <div class="modal-body">
                <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Fullname</b>: ${item.name}</li>
                <li class="list-group-item"><b>Mass</b>: ${item.mass}</li>
                <li class="list-group-item"><b>Gender</b>: ${item.gender}</li>
                <li class="list-group-item"><b>Height</b>: ${item.height}</li>
                <li class="list-group-item"><b>Skin Color</b>: ${item.skin_color}</li>
                <li class="list-group-item"><b>Birth Year</b>: ${item.birth_year}</li>
                <li class="list-group-item"><b>Eye color</b>: ${item.eye_color}</li>
                <li class="list-group-item"><b>Planet</b>: ${JSON.parse(localStorage.getItem('planets')).results[index].name}</li>
                <li class="list-group-item"><b>Created</b>: ${item.created}</li>
                <li class="list-group-item"><b>Edited</b>: ${item.edited}</li>
                <li class="list-group-item"><b>URL</b>: ${item.url}</li>

              </ul>
                </div>
          
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
          
              </div>
            </div>
          </div>
          
            </div>
              `;
              
            })
            .join(' ');
          
          //add the prev/next navigation
          let footer = document.getElementById('footer');
          footer.innerHTML = '';
      
          if (data.previous) {
            //previous link
            let prev = document.createElement('a');
            prev.href = data.previous;
            let url = new URL(data.previous);
            let labels = url.pathname.split('/');
            let label = labels[labels.length - 2];
            prev.textContent = `<< Previous ${label}`;
            prev.setAttribute('data-link', data.previous);
            footer.append(prev);
          }
          if (data.next) {
            //next link
            let next = document.createElement('a');
            next.href = data.next;
            let url = new URL(data.next);
            let labels = url.pathname.split('/');
            let label = labels[labels.length - 2];
            next.textContent = `Next ${label} >>`;
            next.setAttribute('data-link', data.next);
            footer.append(next);
          }
        }
      };
      
      document.addEventListener('DOMContentLoaded', SWAP.init);
      


      
