document.write("<div class='code'>");
//1-
let a = 2, b = 3, result;
result = (a + b < 4) ? "Мало" : "Много";

document.write("a = " + a + " , b = " + b + "<br>");
document.write("result = (a + b < 4) ? \"Мало\" : \"Много\"; = " + result);
document.write("<br><hr>");

//2-
let message;
let login = "Вася";

message = (login === "Вася") ? "Привет" : (login === "Директор") ? "Здравствуйте" : (login === "") ? "Нет логина" : "";
document.write("<strong>"+message+" "+login+"</strong>");
document.write("<br><hr>");
//3-
let A = 1;
let B = 20;
let sum = 0;
for (let i = 0; i <= B; i++) {
    sum += i;
    document.write("<span class='final'>sum = " + sum + "</span><br>");
    if (i % 2 !== 0) {
        document.write("<span class='odd'>Нечётное число :" + i + "</span><br>");
    }
    /*  else {
          document.write("<br>чётное число :"+i+ "<br>");
      }*/
}
document.write("<br><hr>");

//4-*******************************Прямоугольник***************************************************
document.write('<h2>Прямоугольник</h2>');

let columns = 40;
let rows = 10;

for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
        document.write("$");
    }
    document.write("<br>");
}

document.write("<br><hr>");


//5-***************************Прямоугольный треугольник*******************************************
document.write('<h2>Прямоугольный треугольник</h2>');

for (let i = 0; i < rows; i++) {
    for (let j = 0; j < i; j++) {
        document.write("$");
    }
    document.write("<br>");
}
document.write("<br><hr>");


//6-*************************Равносторонний треугольник********************************************
document.write('<h2>Равносторонний треугольник</h2>');

for (let i = 0; i < rows; i++) {
    for (let j = rows; j > i; j--) {
        document.write("&nbsp;");
    }
    for (let k = 0; k < i; k++) {
        document.write("$");
    }
    document.write("<br>");
}

document.write("<br><hr>");


//7-******************************Ромб*************************************************************
document.write('<h2>Ромб</h2>');

for (let i = 0; i < rows; i++) {
    for (let j = rows ; j > i; j--) {
        document.write("&nbsp;");
    }
    for (let k = 0; k < i; k++) {
        document.write("$");
    }
    document.write("<br>");
}

for (let i = 0; i < rows; i++) {
    for (let j = 0; j < i; j++) {
        document.write("&nbsp;");
    }
    for (let k = rows; k > i; k--) {
        document.write("$");
    }
    document.write("<br>");
}

document.write("</div class='code'>");