//********************************* */
let userOne = {
    firstName: "Farid",
    lastName: "Samir",
    age: 35,
    user: function () {
        console.log(`Привет! Я ${this.firstName} ${this.lastName}`)
        return { firstName: this.firstName, lastName: this.lastName }
    }
}

let context = userOne.user();
let firstName = context.firstName,
    lastName = context.lastName;

let userTwo = {
    firstName: "Ivan",
    lastName: "Ivanov",
    age: 32
}

userTwo.user = userOne.user.bind(userTwo);
userTwo.user();
//*********************************** */
let arr1 = new Array(1, 2, 3, 4, 5);
    Array.prototype.multi = function(number) {
            this.forEach(function (v, i, a) { a[i] = v * number });
            return this;
        }  

    arr1.multi(3);
    document.write(arr1+"<br/>");
    
    let arr2 = [10, 20, 30, 40];
    arr2.multi(2);
    document.write(arr2+"<br/>");

//********************************* */
const user = {
    name: "Ivan",
    age: 28
};
Object.freeze(user);
user.name = "Peter";
console.log(user);
//********************* */
const user2 = {
    name:"Ivan",
    surname : "Ivanov",

    get userInfo(){
        return `${this.name} ${this.surname}`;
    },

    set userInfo(fullName){
        [this.name, this.surname] = fullName.split(" ");
    }
};
console.log("Getting old values : ",user2.userInfo);
//setting userInfo
user2.userInfo = "Peter Petrov";
console.log("Setting new values : ",user2.userInfo);