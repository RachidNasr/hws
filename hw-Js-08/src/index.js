//1-Домашнее задание
const circlesGenarator = document.querySelector(".btn")
circlesGenarator.onclick = () => {
    let diameter = parseInt(prompt('Диаметр круга'));
    if (diameter > 0) {
        for (let i = 0; i < 100; i++) {
            let container = document.querySelector(".container");
            let circle = document.createElement("input");
            circle.classList.add('btnCircle');
            circle.setAttribute("type", "button");
            circle.setAttribute("value", `    `);
            circle.setAttribute("onclick", "selfRemove(this)");
            circle.style.backgroundColor = `#${(Math.random() * 0xFFFFFF << 0).toString(16)}`;
            circle.style.padding = `${diameter}px`;
            container.append(circle);
        };
    }
};
const selfRemove = (param) => param.remove();

//2-Классная работа

const data = {
    "date": "28.10.2021",
    "bank": "PB",
    "baseCurrency": 980,
    "baseCurrencyLit": "UAH",
    "exchangeRate": [{
        "baseCurrency": "UAH",
        "saleRateNB": 19.8206000,
        "purchaseRateNB": 19.8206000
    }, {
        "baseCurrency": "UAH",
        "currency": "AZN",
        "saleRateNB": 15.6620000,
        "purchaseRateNB": 15.6620000
    }, {
        "baseCurrency": "UAH",
        "currency": "BYN",
        "saleRateNB": 10.8949000,
        "purchaseRateNB": 10.8949000
    }, {
        "baseCurrency": "UAH",
        "currency": "CAD",
        "saleRateNB": 21.2560000,
        "purchaseRateNB": 21.2560000
    }, {
        "baseCurrency": "UAH",
        "currency": "CHF",
        "saleRateNB": 28.7252000,
        "purchaseRateNB": 28.7252000,
        "saleRate": 29.2000000,
        "purchaseRate": 27.6000000
    }, {
        "baseCurrency": "UAH",
        "currency": "CNY",
        "saleRateNB": 4.1259000,
        "purchaseRateNB": 4.1259000
    }, {
        "baseCurrency": "UAH",
        "currency": "CZK",
        "saleRateNB": 1.1925000,
        "purchaseRateNB": 1.1925000,
        "saleRate": 1.3000000,
        "purchaseRate": 1.1000000
    }, {
        "baseCurrency": "UAH",
        "currency": "DKK",
        "saleRateNB": 4.1193000,
        "purchaseRateNB": 4.1193000
    }, {
        "baseCurrency": "UAH",
        "currency": "EUR",
        "saleRateNB": 30.6447000,
        "purchaseRateNB": 30.6447000,
        "saleRate": 30.8500000,
        "purchaseRate": 30.2500000
    }, {
        "baseCurrency": "UAH",
        "currency": "GBP",
        "saleRateNB": 36.2551000,
        "purchaseRateNB": 36.2551000,
        "saleRate": 37.2000000,
        "purchaseRate": 35.1000000
    }, {
        "baseCurrency": "UAH",
        "currency": "GEL",
        "saleRateNB": 8.5403000,
        "purchaseRateNB": 8.5403000
    }, {
        "baseCurrency": "UAH",
        "currency": "HUF",
        "saleRateNB": 0.0840860,
        "purchaseRateNB": 0.0840860
    }, {
        "baseCurrency": "UAH",
        "currency": "ILS",
        "saleRateNB": 8.2648000,
        "purchaseRateNB": 8.2648000
    }, {
        "baseCurrency": "UAH",
        "currency": "JPY",
        "saleRateNB": 0.2318900,
        "purchaseRateNB": 0.2318900
    }, {
        "baseCurrency": "UAH",
        "currency": "KZT",
        "saleRateNB": 0.0617800,
        "purchaseRateNB": 0.0617800
    }, {
        "baseCurrency": "UAH",
        "currency": "MDL",
        "saleRateNB": 1.5052000,
        "purchaseRateNB": 1.5052000
    }, {
        "baseCurrency": "UAH",
        "currency": "NOK",
        "saleRateNB": 3.1463000,
        "purchaseRateNB": 3.1463000
    }, {
        "baseCurrency": "UAH",
        "currency": "PLN",
        "saleRateNB": 6.6326000,
        "purchaseRateNB": 6.6326000,
        "saleRate": 6.7900000,
        "purchaseRate": 6.4900000
    }, {
        "baseCurrency": "UAH",
        "currency": "RUB",
        "saleRateNB": 0.3761700,
        "purchaseRateNB": 0.3761700,
        "saleRate": 0.3850000,
        "purchaseRate": 0.3550000
    }, {
        "baseCurrency": "UAH",
        "currency": "SEK",
        "saleRateNB": 3.0728000,
        "purchaseRateNB": 3.0728000
    }, {
        "baseCurrency": "UAH",
        "currency": "SGD",
        "saleRateNB": 19.5603000,
        "purchaseRateNB": 19.5603000
    }, {
        "baseCurrency": "UAH",
        "currency": "TMT",
        "saleRateNB": 7.6050000,
        "purchaseRateNB": 7.6050000
    }, {
        "baseCurrency": "UAH",
        "currency": "TRY",
        "saleRateNB": 2.7775000,
        "purchaseRateNB": 2.7775000
    }, {
        "baseCurrency": "UAH",
        "currency": "UAH",
        "saleRateNB": 1.0000000,
        "purchaseRateNB": 1.0000000
    }, {
        "baseCurrency": "UAH",
        "currency": "USD",
        "saleRateNB": 26.3712000,
        "purchaseRateNB": 26.3712000,
        "saleRate": 26.6500000,
        "purchaseRate": 26.2500000
    }, {
        "baseCurrency": "UAH",
        "currency": "UZS",
        "saleRateNB": 0.0024895,
        "purchaseRateNB": 0.0024895
    }]
};

const root = document.querySelector("#root");
const table = document.createElement('table');
table.style.textAlign = 'center';
table.style.border = '1px solid black';
table.style.borderCollapse = 'collapse';
table.style.borderSpacing = '0px';
root.prepend(table);

data.exchangeRate.forEach((item, index) => {
    console.log(item);
    const tr = document.createElement('tr');
    table.append(tr);
    for (let key in item) {
        if (index === 0) {
            continue;
        }
        else if (index === 1) {
            const el = document.createElement('th');
            el.innerHTML = `${key}`;
            console.log(key);
            tr.append(el);
            tr.style.color = '#FFFFFF';
            tr.style.backgroundColor = '#1B5E20';
            el.style.border = '1px solid black';
        } else {
            const el = document.createElement('td');
            el.innerHTML = `${item[key]}`;
            tr.style.color = '#1B5E20';
            tr.style.backgroundColor = '#B9F6CA';
            tr.append(el);
            el.style.border = '1px solid black';
        }
    }
    });