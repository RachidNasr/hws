document.write("<div class='code'>");

//1-Создайте массив styles
document.write("<span class='oblique'>1-Создайте массив styles</span><br>");
let styles = ["Джаз", "Блюз"];
document.write("[" + styles + "]");
document.write("<br><br>");

//2-Добавьте «Рок-н-ролл» в конец. push() method
document.write("<span class='oblique'>2-Добавьте «Рок-н-ролл» в конец. </span><br>");
let add = styles.push("Рок-н-ролл");
document.write("[" + styles + "]");
document.write("<br><br>");


//3-Замените значение в середине на «Классика»
document.write("<span class='oblique'>3-Замените значение в середине на «Классика». </span><br>");
let midlle = styles.length / 2;
styles.splice(Math.floor(midlle) - 1, 1, "Классика");
document.write("[" + styles + "]");
// console.log(Math.floor(midlle));
document.write("<br><br>");

//4-Удалите первый элемент массива и покажите его.
document.write("<span class='oblique'>4-Удалите первый элемент массива и покажите его.</span><br>");
let remove = styles.shift();
document.write("[" + styles + "]");
document.write("<br>");
document.write("Удаленный элемент: " + remove);
document.write("<br><br>");


//5- Вставьте «Рэп» и «Регги» в начало массива.
document.write("<span class='oblique'>5- Вставьте «Рэп» и «Регги» в начало массива.</span><br>");
let insert = styles.unshift("Рэп", "Регги");
document.write("[" + styles + "]");
document.write("<sub>Рашид Наср / fe-pro-220921</sub>");

document.write("</div>");