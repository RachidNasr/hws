
//Js operators precedence  x++//x--//+x//-x//++x//--x//x**y//x * y//x / y// x % y// x + y// x - y
//declare the variables and initiate them
document.write("<div class='code'>");
var x = 6;
var y = 14;
var z = 4;

x += y - x++ * z;  //x = (x + (y - (x++ * z)) = 6+(14-(6*4))  = 6+(14-24) = 6-10 = -4
document.write("x += y - x++ * z = " + "<strong>" + x + "</strong>" + "<hr>");
console.log(x);
// 1- (x++) -> (x = 7) -> first use then increment 6 will be used
// 2- (x++ * z) -> (6 * 4 = 24)
// 3- (y - 24) -> (14 - 24 = -10)
// 4- (6 + (-10)) = -4

// set up the variables to initial values
x = 6;
y = 14;
z = 4;

z = --x - y * 5;    //z = (6-1)-(14*5) = 5-70 = -65
document.write("z = --x - y * 5 = " + "<strong>" + z + "</strong>" + "<hr>");
console.log(z);
// 1- (--x) -> (x = 5)
// 2- (y * 5) -> ( 14 * 5 = 70)
// 3- (5 - 70)-> ( - 65)

// set up the variables to initial values
x = 6;
y = 14;
z = 4;

y /= x + 5 % z; // y = y /( x + (5 % 4))= 14/(6+1) = 14/7 = 2
document.write("y /= x + 5 % z = " + "<strong>" + y + "</strong>" + "<hr>");
console.log(y);
//1- (5 % z) -> (5 % 4 = 1)
//2- (x + 1) -> (6 + 1 = 7)
//3- (y / 7) -> (14 / 7) = 2




// set up the variables to default values
x = 6;
y = 14;
z = 4;
var result;
result = z - x++ + y * 5; // result = (4 - 6) +(14*5) = -2 + 70 = 68
document.write("z - x++ + y * 5 = " + "<strong>" + result + "</strong>" + "<hr>");
console.log(result);
//1- x++ ( x = 7)
//2- y * 5 ( 14 * 5 = 70)
//3-  z - 6 ( 4 - 6  = -2)
//4- -2 + 70 = 68



// set up the variables to initial values
x = 6;
y = 14;
z = 4;

x = y - x++ * z; // x = 14 - (6*4) = 14-24 = -10
document.write("x = y - x++ * z = " + "<strong>" + x + "</strong>" + "<hr>");
console.log(x);
//1- x++ ( x = 7)
//2- 6 * z ( 6 * 4 = 24)
//3- 3. y - 24 ( 14 - 24  = -10)

document.write("</div>");